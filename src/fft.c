#include <stdio.h>
#include <math.h>
#define SWAP(a,b) tempr = (a); (a) = (b); (b) = tempr
#define pi 3.14159265358979323846264338327950288419716939937510

void four1 (float data[], unsigned long nn, int isign)
{
	/*
    Replace data[1..2*nn] by its discrete fourier transform if isign is 1.
    or replace data[1..2*nn] by nn times its inverse fourier transform if
    isign is -1. Data is a complex array of length nn or equivalently, a 
    real array of length 2*nn. nn must be an integer power of 2. (This is 
    not checked for).
	(Numerical Recipes in C, 2nd Edition, P504)
	*/
	unsigned long n;
	unsigned long mmax;
	unsigned long m;
	unsigned long j;
	unsigned long istep;
	unsigned long i;

	double wtemp;
	double wr;
	double wpr;
	double wpi;
	double wi;
	double theta;

	float tempr;
	float tempi;

	n = nn << 1;
	j = 1;

	for (i = 1; i < n; i += 2) {
			// bit reversal, exchanges the two complex numbers
			if (j > i) {
					SWAP(data[j], data[i]);
					SWAP(data[j+1], data[i+1]);
			}
			m = n >> 1;
			while (m >= 2 && j > m)	{
					j -= m;
					m >>= 1;
			}
			j += m;
	}
	mmax = 2;
	while (n > mmax) {
		istep = mmax << 1;
		theta = isign * (6.28318530717959 / mmax);
		wtemp = sin(0.5 * theta);
		wpr = -2.0 * wtemp * wtemp;
		wpi = sin(theta);
		wr = 1.0;
		wi = 0.0;

		for (m = 1; m < mmax; m += 2) {
			for (i = m; i <= n; i += istep) {
				// Daniel-Lanczos formula
				j = i + mmax;
				tempr = wr * data[j] - wi * data[j + 1];
				tempi = wr * data[j + 1] + wi * data[j];
				data[j] = data[i] - tempr;
				data[j + 1] = data[i + 1] - tempi;
				data[i] += tempr; 
				data[i + 1] += tempi;
			}
			// trigonometric recurrence
			wr = (wtemp = wr) * wpr - wi * wpi + wr;
			wi = wi * wpr + wtemp * wpi + wi;
		}
		mmax = istep;
	}
}

void wave_packet (FILE* output_file)
{
	long nn = 256;			// samping rate, has to be power of 2
	
	float k_i = 0.0;		// imaginary constant i
	float k = 0.0;			// wavevector k
	
	float t = 0.0;			// time t
	float dt = 0.001;
	float t_min = 0.0;
   	float t_max = 2.5;
	
	float x = 0.0;			// displacement x
	float dx = 30.0 / 256.0;
	float x_min = -15.0;
	float x_max = 15.0;
	
	// misc
	int i = 0;
	int steps = (t_max - t_min) / dt;
	
	// wavepacket
	float V = 0.0;					// potential V(x)
	float psi = 0.0;				// wavefunction psi
	float norm = 1.0;				// normalization factor
	float potential_psi[steps];		// product of potential and psi
	float wave[steps];				// wavepacket

	fprintf(output_file, "# x t psi\n");
	for (x = x_min; x <= x_max; x += dx) {
		for (i = 0; i <= (steps - 1); i++) {
			// multiplying exponential factor V(x) with psi
			V = 5 * exp(-10 * pow(x, 2) * dt);
			psi =  norm * exp(-0.1 * pow((x + 5), 2)) * exp(k_i * k * x);
			potential_psi[i] = exp(-k_i * V) * psi;
		}
		// fourier transform potential_psi to momentum space
		four1(potential_psi, nn, 1);

		for (i = 0; i <= (steps - 1); i++) {
			// exponential factor involving k multiplied by newly fourier 
			// transformed potential_psi
			wave[i] = exp(-k_i * pow(k, 2) * dt) * potential_psi[i];
		}	
		// inverse fourier transform the wave back to x space 
		four1(wave, nn, -1);
	
		t = 0; // reset time t for different x
		// write results into data file
		for (i = 0; i <= (steps - 1); i++) {
			fprintf(output_file, "%.3f %.3f %.3f\n", x, t, wave[i]);
			t += dt; 
		}
	}
}

void fourier_transform_test (FILE* output_file)
{
	/* 
	This function serves as a test to see whether my implementation of the
	fft is working or not.
	*/
	int n = 30;		// number of samples
	float x[n];		// array that holds all values for x		

	// fourier transform samples
	int N1 = 64;
	int N2 = 128;
	int N3 = 256;

	// fourier transformed results
	float X1[N1];
	float X2[N2];
	float X3[N3];

	// misc
	int i = 0;

	printf("Running fourier transform tests...\n");
  	fprintf(output_file, "# x t\n"); 
	for (i = 0; i <= (n - 1); i++) 
		x[i] = cos((2 * pi * i) / 10);
	
	four1(x-1, 64, 1);
	for (i = 0; i <= (n - 1); i++)
		fprintf(output_file, "%f\n", x[i]);

}

int main (int argc, char *argv[]) 
{
	// open data_file to write results
	FILE* file; 
	if (argc == 1)
		file = fopen("results.dat", "w");
	else
		file = fopen(argv[1], "w");

	//wave_packet(file);	
	fourier_transform_test(file);

	return 0;
} 



