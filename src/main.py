#!/bin/python
from cmath import *
from fft import *
from numpy import multiply
import Gnuplot

""" 
The normal range() function does not allow increments in float type, this simple function 
merely emulates range() but with float support 
"""
def frange (min_value, max_value, step):
	array = []
	value = float(min_value)
	while value < float(max_value):
		array.append(value)
		value += float(step)
	return array

""" Serves as a simple fourier transform test by fourier transforming a cos function """
def cosine_fft_test (data_save_path, plot_save_path):
	N = 32.0
	x = []
	x_min = 0
	x_max = 30
	x_min_max_diff = x_max - x_min
	dx = x_min_max_diff / N
	k_i = sqrt(-1)
	
	# data files
	file_1 = open(data_save_path + 'normal_cos.dat', 'w')
	file_2 = open(data_save_path + 'fourier_transformed_cos.dat', 'w')
	file_3 = open(data_save_path + 'inverse_fourier_transformed_cos.dat', 'w')
	file_1.write('#x y\n')
	file_2.write('#x y\n')
	file_3.write('#x y\n')
	
	# gnuplot settings
	gp = Gnuplot.Gnuplot(persist=1)
	gp("set terminal png")
	
	# cos function
	cache = multiply((2.0 * pi), multiply(0.1, frange(x_min, x_max, dx)))
	for i in range(len(cache)): x.append(cos(cache[i]))

	# fft and ifft
	X = fft(x)
	Y = ifft(X)

	for i in range(int(N)):
		j = i / float(N)
		file_1.write('%f %f\n' % (j, abs(x[i])))
		file_2.write('%f %f\n' % (j, abs(X[i])))
		file_3.write('%f %f\n' % (j, abs(Y[i])))
	file_1.close
	file_2.close
	file_3.close
	
	gp('set output "' + plot_save_path + 'normal_gausian' + '.png"')
	gp('plot "' + data_save_path + 'normal_gausian.dat' + '" with lines')
	gp('set output "' + plot_save_path + 'fourier_transformed_gausian' + '.png"')
	gp('plot "' + data_save_path + 'fourier_transformed_gausian.dat' + '" with lines')
	gp('set output "' + plot_save_path + 'inverse_fourier_transformed_gausian' + '.png"')
	gp('plot "' + data_save_path + 'inverse_fourier_transformed_gausian.dat' + '" with lines')

	return 0

""" Serves as a simple fourier transform test by fourier transforming a gausian function """
def gausian_fft_test (data_save_path, plot_save_path):
	y = []
	N = 1024.0 # number of fft steps (aka sampling rate)	
	x_min = -5.0
	x_max = 5.0	
	x_min_max_diff = x_max - x_min
	dx = x_min_max_diff / N

	# data files
	file_1 = open(data_save_path + 'normal_gausian.dat', 'w')
	file_2 = open(data_save_path + 'fourier_transformed_gausian.dat', 'w')
	file_3 = open(data_save_path + 'inverse_fourier_transformed_gausian.dat', 'w')
	file_1.write('#x y\n')
	file_2.write('#x y\n')
	file_3.write('#x y\n')
	
	# gnuplot settings
	gp = Gnuplot.Gnuplot(persist=1)
	gp("set terminal png")
	
	# gausian function
	x = frange(x_min, x_max, dx)
	cache = -multiply(x, x)
	for i in cache: y.append(exp(i))
	
	# fft and ifft
	Y = fft(y)
	Z = ifft(Y)

	for i in range(int(N)):
		j = i / float(N)
		file_1.write('%f %f\n' % (j, abs(y[i])))
		file_2.write('%f %f\n' % (j, abs(Y[i])))
		file_3.write('%f %f\n' % (j, abs(Z[i])))
	file_1.close
	file_2.close
	file_3.close
	
	gp('set output "' + plot_save_path + 'normal_gausian' + '.png"')
	gp('plot "' + data_save_path + 'normal_gausian.dat' + '" with lines')
	gp('set output "' + plot_save_path + 'fourier_transformed_gausian' + '.png"')
	gp('plot "' + data_save_path + 'fourier_transformed_gausian.dat' + '" with lines')
	gp('set output "' + plot_save_path + 'inverse_fourier_transformed_gausian' + '.png"')
	gp('plot "' + data_save_path + 'inverse_fourier_transformed_gausian.dat' + '" with lines')

	return 0

""" Solving the split-step fourier of the Schrodinger Equation """
def schrodinger_equation (data_save_path, plot_save_path):
	k_i = sqrt(-1)	# imaginary constant i
	k = []			# wavevector k of the kinetic operator
	k_0 = 2.5	    # intial wavevector k for psi
	               
	# time t
	t = 0		   
	dt = 0.01
	t_min = 0.0
   	t_max = 1
	
	# displacement x 
	x = []		   
	dx = 30.0 / 256.0
	x_min = -15.0
	x_max = 15.0
	
	# wavepacket
	norm = 1.0			# normalization factor
	V = 0.0				# potential V(x)
	psi = 0.0			# wavefunction psi
	potential_psi = []	# product of potential and psi
	wave = []			# wavepacket 
	P = []              # fft-ed of potential_psi
	W = []				# fft-ed of everything

	# gunplot settings
	gp = Gnuplot.Gnuplot(persist=1)
	gp("set terminal png")
	gp("set xrange [-15:15]")
	gp("set yrange [-1:1]")

	# intializing simulation
	print "Starting the simulation"
	print "Initiating..."
	# python stdlib range() does not support type float so wrote own frange
	x = frange(x_min, x_max, dx)
	k = frange(-pi/dx, pi/dx, dx)
		
	# executing simulation
	print "Executing!"
	print "This FFT does " + str(len(x)) + " steps"
	counter = 1
	while t < t_max:
		# checks the number of digits in counter to add sufficient 0's infront
		if len(str(counter)) == 3: file_name = 'schrodinger_0%i' % (counter)
		elif len(str(counter)) == 2: file_name = 'schrodinger_00%i' % (counter)
		elif len(str(counter)) == 1: file_name = 'schrodinger_000%i' % (counter)
		else: file_name = 'schrodinger_%i' % (counter)
		# creates data file to log data
		output_file = open(data_save_path + file_name + ".dat", "w")
		output_file.write("# x psi\n")
		# set the plot name
		gp('set output "' + plot_save_path + file_name + '.png"')

		print "at t = %.3f" % (t)
		for i in x:	 
			# multiplying exponential factor V(x) with psi
			V = 5.0 * exp(-10.0 * pow(i, 2.0) * t)
			psi = norm * exp(-0.1 * pow((i + 5.0), 2.0)) * exp(k_i * k_0 * i)
			potential_psi.append(multiply(exp(multiply(-k_i, V)), psi))
		
		P = fft(potential_psi) # first fft with pontential and psi 
		
		# exponential involving k multiplied by fourier transformed potential_psi
		for i in range(len(x)):
			wave.append(multiply(exp(-k_i * pow(k[i], 2) * t), P[i]))
		W = ifft(wave) # inverse fourier transfrom everything back to x-space
			
		index = 0
		for i in x:	
			output_file.write('%.4f  %.4f\n' % (i, W[index]))
			index += 1

		potential_psi = [] # resets the array so the length of the array does not increase
		wave = [] # resets the array so the length of the array does not increase
		output_file.close()
		# plot and save it to a png file
		gp('plot "' + data_save_path + file_name + '.dat" with lines')
		counter += 1
		t += dt
	return 0


data_save_path = "/home/chris/projects/wave_packet/data/"
plot_save_path = "/home/chris/projects/wave_packet/images/"
#cosine_fft_test(data_save_path, plot_save_path)
gausian_fft_test(data_save_path, plot_save_path)
#schrodinger_equation(data_save_path, plot_save_path)
